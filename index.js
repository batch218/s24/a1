/*
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
*/

/* [Exponent Operator and Template Literals]
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/

//insert code here...

const getCube = 2 ** 3;
console.log(`The Cube Root of 2 is ${getCube}`);




/* [Array Destructuring and Template Literals]
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/

//insert code here...
const fullAddress = ["258 Washington", "Ave NW","California 90011"];
const [streetName, addressName,City ] = fullAddress;
console.log(`I live at ${streetName} ${addressName}, ${City}`);


/* [Object Destructuring and Template Literals]
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/
const animal = {
    animalName : "Lolong",
    animalType : "crocodile",
    height : "20 ft 3 in",
    weight : 1075
};
console.log(`${animal.animalName} was a water ${animal.animalType}. He weighed at ${animal.weight} kgs with a measurement of ${animal.height}.   `);

/* [forEach() method and Implicit Return(arrow function => )]
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/
const numbers = [1,2,3,4,5];


numbers.forEach(printOut => console.log(`${printOut}`));

/* [reduce() method and Implicit Return(arrow function => )]
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers from array of numbers of instuction #9.
*/

const sum_reducer = (accumulator, currentValue) => accumulator + currentValue;

// let total = add(1,2);

let sum = numbers.reduce(sum_reducer);
console.log(`${sum}`);


/*  [Class-Based Object Blueprint / JavaScript Classes]
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/
class Dog{
  constructor(name,age,breed){
    this.name = name;
    this.age = age ;
    this.breed = breed ;
  }
}
const myPet = new Dog();
// Reassigning value of each property
myPet.name = "Frankie";
myPet.age = 5;
myPet.breed = "Miniature Dachshund";
console.log(myPet);
// 14. Create a git repository named S24.